package task.simulator

import pc.modelling.CTMC
import scala.util.Random

/**
  * Enumeration of states
  */
object State extends Enumeration {
  val idle,send,done,fail = Value
}
import State._

/**
  * This class implements a simulator to calculate the average time at which communication is done and the average
  * relative time the system is in fail state, until communication is done, across n runs.
  *
  * @tparam A - generics type of states
  *
  * @author Bottazzi Manuel
  *         Lucchi Giulia
  *         Pecorelli Margherita
  */
case class simulator[A]() {

  /**
    * Returns the average time at which communication is done, across n runs.
    *
    * @param channel - is the CTMC
    * @param state - is the initial state
    * @param n - is the number of runs
    * @return the average time.
    */
  def averageTime(channel: CTMC[State.Value], state: A, n: Int): Double = {
    average(channel, state, n, timeToDone)
  }

  /**
    * Returns the average relative time the system is in fail state, until communication is done, across n runs.
    *
    * @param channel - is the CTMC
    * @param state - is the initial state
    * @param n - is the number of runs
    * @return the average relative time.
    */
  def averageRelativeFailTime(channel: CTMC[State.Value], state: A, n: Int): Double = {
    average(channel, state, n, relativeTime)
  }

  /**
    * Returns the average time.
    *
    * @param channel - is the CTMC
    * @param state - is the initial state
    * @param n - is the number of runs
    * @param f - the function to call for time calculation
    * @return the average time.
    */
  private def average(channel: CTMC[State.Value], state: A, n: Int, f : (CTMC[State.Value], A) => Double): Double = {
    var sumTime: Double = 0
    for(i <- 0 until n) {
      val time: Double = f(channel, state)
      sumTime += time
    }
    sumTime/n
  }

  /**
    * Returns the relative time the system is in fail state.
    *
    * @param channel - is the CTMC
    * @param state - is the initial state
    * @return the relative time the system is in fail state.
    */
  private def relativeTime(channel: CTMC[State.Value], state: A): Double = {
    var failTime: Double = 0
    var untilDone = channel.simulation(idle, new Random()).takeWhile(a => a._2 != done).toList
    for (i <- 0 until untilDone.length) {
      if(untilDone(i)._2 == State.fail) failTime += untilDone(i+1)._1 - untilDone(i)._1
    }
    (failTime/timeToDone(channel, state))*100

  }

  /**
    * Returns the time at which communication is done.
    *
    * @param channel - is the CTMC
    * @param state - is the initial state
    * @return the time at which communication is done.
    */
  private def timeToDone(channel: CTMC[State.Value], state: A): Double = {
    channel.simulation(idle, new Random()).filter(a => a._2 == done).take(1).toList.head._1
  }

}

object main extends App {
  val channel: CTMC[State.Value] = CTMC.ofTransitions(
    (idle,1.0,send),
    (send,100000.0,send),
    (send,100000.0,done),
    (send,200000.0,fail),
    (fail,100000.0,idle),
    (done,1.0,done)
  )

  println(simulator().averageTime(channel, idle, 100))
  println(simulator().averageRelativeFailTime(channel, idle, 100))
}
